let employees = [
	{
		"name": "Thonie Fernandez",
		"department": "Instructor",
		"yearEmployed": "2020",
		"ratings": 5.0
	},
	{
		"name": "Charles Quimpo",
		"department": "Instructor",
		"yearEmployed": "2010",
		"ratings": 5.0
	},
	{
		"name": "Martin Miguel",
		"department": "Instructor",
		"yearEmployed": "2019",
		"ratings": 4.0
	},
	{
		"name": "Alvin Estive",
		"department": "Instructor",
		"yearEmployed": "2020",
		"ratings": 5.0
	}
]
console.log(employees);

let user = {
	"name": "kyle",
	"favoriteNUmber": 7,
	"isProgrammer": true,
	"hobbies": [
				"Weight Lifting",
				"Reading Comics",
				"Playing the Guitar"
				],
	"friends": [
				{
					"name": "Robin",
					"isProgrammer": true
				},
				{
					"name": "Daniel",
					"isProgrammer": false
				},
				{
					"name": "Oswald",
					"isProgrammer": true
				}
				]
}

console.log(user)

let application = `{
	"name": "javascript server",
	"version": "1.0",
	"description": "server side application done using javascript and js",
	"main": "index.js",
	"scripts": {
		"start": "node index.js"
	},
	"keywords": [
		"server",
		"node",
		"backend"
	],
	"author": "John Smith",
	"license": "ISC"
}`;
console.log(application);
console.log(typeof application);

console.log(employees);
console.log(JSON.stringify(employees));
console.log(JSON.parse(application));
console.log(JSON.parse(application).name);